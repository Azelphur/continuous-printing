#!/bin/bash

sudo apt-get update
sudo apt-get -y install python3-opencv python3-yaml python3-pip
python3 -m pip install imutils
git clone https://gitlab.com/Azelphur/continuous-printing.git
cd continuous-printing
cp config.yaml.example config.yaml
sudo service webcamd stop
echo "I have disabled the webcam functionality in octoprint"
echo "Only one application can access the webcam at a time"
echo "And you for motion detection, we need access to the camera"
echo "To re-enable camera support in octoprint, you can run"
echo "sudo service webcamd start"
