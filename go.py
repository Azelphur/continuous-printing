#!/usr/bin/python

import yaml
import requests
import time
import logging
import imutils
import cv2
import sys
import os
from imutils.video import VideoStream


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.CRITICAL)
console_handler.setFormatter(
    logging.Formatter("%(asctime)s [%(levelname)s](%(name)s): %(message)s")
)
logger.addHandler(console_handler)


class Octoprint:
    JOB_PATH = "/api/job"
    FILE_PATH = "/api/files"
    BED_PATH = "/api/printer/bed"
    TOOL_PATH = "/api/printer/tool"

    def __init__(self, url, api_key):
        self.session = requests.Session()
        self.api_key = api_key
        self.url = url.rstrip("/")

    def request(self, verb, path, json={}):
        headers = {"X-Api-Key": self.api_key}
        logger.debug("Sending %s request to %s%s", verb, self.url, path)
        return self.session.request(verb, self.url + path, headers=headers, json=json)

    def get(self, path):
        return self.request("GET", path)

    def post(self, path, json={}):
        return self.request("POST", path, json=json)

    def get_current_job(self):
        return self.get(self.JOB_PATH).json()

    def is_ready(self):
        job = self.get_current_job()
        if job["state"] == "Operational":
            return True
        return False

    def start_printing(self, file_name):
        logger.info("Starting print %s", file_name)
        json = {"command": "select", "print": True}
        r = self.post(self.FILE_PATH + file_name, json=json)
        logger.info("Octoprint returned code %s", r.status_code)
        r.raise_for_status()

    def wait_until_ready(self):
        logger.info("Waiting for print to finish...")
        while True:
            if self.is_ready():
                break
            time.sleep(10)

    def wait_for_cooldown(self, temperature):
        logger.info("Waiting for cooldown...")
        while True:
            bed = self.get(self.BED_PATH).json()
            if bed["bed"]["actual"] <= temperature:
                return
            time.sleep(10)

    def cooldown(self):
        logger.info("Cooling down hotend")
        data = {"command": "target", "targets": {"tool0": 0}}
        self.post(self.TOOL_PATH, data)


class Motion:
    def __init__(self, src, min_area):
        self.src = src
        self.min_area = min_area

    def wait_for_motion(self, timeout, test_mode=False, headless=False):
        logger.info("Waiting for motion...")
        start_time = time.time()
        vs = VideoStream(src=self.src).start()
        firstFrame = None

        i = 0
        while True:
            frame = vs.read()
            # resize the frame, convert it to grayscale, and blur it
            frame = imutils.resize(frame, width=500)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (21, 21), 0)
            # if the first frame is None, initialize it
            if firstFrame is None:
                firstFrame = gray
                continue
            # compute the absolute difference between the current frame and
            # first frame
            frameDelta = cv2.absdiff(firstFrame, gray)
            thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]
            # dilate the thresholded image to fill in holes, then find contours
            # on thresholded image
            thresh = cv2.dilate(thresh, None, iterations=2)
            cnts = cv2.findContours(
                thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
            )
            cnts = imutils.grab_contours(cnts)
            text = "Waiting..."
            # loop over the contours
            for c in cnts:
                # if the contour is too small, ignore it
                if cv2.contourArea(c) < self.min_area:
                    continue
                # compute the bounding box for the contour, draw it on the frame,
                # and update the text
                if test_mode and not headless:
                    (x, y, w, h) = cv2.boundingRect(c)
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    text = "Motion Detected"
                else:
                    cv2.destroyAllWindows()
                    vs.stop()
                    return True

            if test_mode and not headless:
                # draw the text and timestamp on the frame
                cv2.putText(
                    frame, text, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2
                )
                # show the frame and record if the user presses a key
                cv2.imshow("Motion", frame)
                key = cv2.waitKey(1) & 0xFF
                if key == ord("q"):
                    break
            if time.time() - start_time > timeout and test_mode == False:
                return False
        # cleanup the camera and close any open windows
        cv2.destroyAllWindows()
        vs.stop()
        return False


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Fully automated printing")
    parser.add_argument("--config", default="config.yaml")
    parser.add_argument(
        "--skip-motion",
        action="store_const",
        default=False,
        const=True,
        help="Skip motion detection, use this at your own peril. Expect spaghetti monsters or worse.",
    )
    parser.add_argument(
        "--already-printing",
        action="store_const",
        default=False,
        const=True,
        help="Wait for current print to finish, eject and then print repeat file",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        help="verbose level... repeat up to three times.",
    )
    parser.add_argument(
        "--test-motion",
        action="store_const",
        default=False,
        const=True,
        help="Test motion detection. Will open a window with a camera feed, won't actually print anything.",
    )
    parser.add_argument(
        "--test-motion-headless",
        action="store_const",
        default=False,
        const=True,
        help="Test motion detection without opening a window, just print results.",
    )
    parser.add_argument(
        "--start-repeat",
        action="store_const",
        default=False,
        const=True,
        help="Start with the repeat file, skipping the priming line. Useful for remotely restarting the process",
    )

    args = parser.parse_args()

    if not args.verbose:
        console_handler.setLevel("ERROR")
    elif args.verbose == 1:
        console_handler.setLevel("WARNING")
    elif args.verbose == 2:
        console_handler.setLevel("INFO")
    elif args.verbose >= 3:
        console_handler.setLevel("DEBUG")

    with open(args.config, "r") as f:
        # Support older versions of yaml (like on octopi)
        if hasattr(yaml, "FullLoader"):
            config = yaml.load(f, Loader=yaml.FullLoader)
        else:
            config = yaml.load(f)

    motion = Motion(config["video_source"], config["motion_threshold"])
    if args.test_motion or args.test_motion_headless:
        print("Checking for motion")
        if motion.wait_for_motion(
            config.get("motion_time", 60), True, args.test_motion_headless
        ):
            print("Motion detected")
        sys.exit(0)

    num_parts = 0
    if "count_file" in config:
        with open(config["count_file"], "r") as f:
            num_parts = int(f.read())

    if args.already_printing:
        logger.info("--already_printing depricated, now handled automatically")

    octoprint = Octoprint(config["url"], config["api_key"])
    if octoprint.is_ready():
        if args.start_repeat:
            octoprint.start_printing(config["repeat_file"])
        else:
            octoprint.start_printing(config["first_file"])

    while True:
        octoprint.wait_until_ready()
        num_parts += 1
        if "exec_print_finished" in config:
            os.system(config["exec_print_finished"])

        if "count_file" in config:
            with open(config["count_file"], "w") as f:
                f.write(str(num_parts))

        logger.info("Print #%d finished, waiting for cooldown", num_parts)
        if config.get("eject_bed_temp", None):
            octoprint.wait_for_cooldown(config["eject_bed_temp"])
        logger.info("Cooldown finished, running eject gcode")
        motion_detected = False
        for i in range(0, config.get("eject_retries", 1)):
            octoprint.start_printing(config["eject_file"])
            if args.skip_motion:
                motion_detected = True
                break
            elif motion.wait_for_motion(config.get("motion_time", 60)):
                logger.info("Motion detected")
                motion_detected = True
                break
        if not motion_detected:
            logger.info("No motion detected. Ejection failed? shutting down")
            octoprint.cooldown()
            if "exec_fail" in config:
                os.system(config.get("exec_fail", None))
            sys.exit(1)
        octoprint.wait_until_ready()
        octoprint.start_printing(config["repeat_file"])
