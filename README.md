This repo is for a piece of software I wrote to assist with continuous printing by verifying that the prints ejected correctly using motion detection. It also might set fire to your printer. YMMV, no warranty, etc. A video demonstration is available at https://youtu.be/DaG44rSTKPw

Requirements:
---
- An i3-style 3D printer, mine is an MK3S. You may be able to do this on other printers.
- Octoprint
- A webcam

How it works:
---
You will need to create three gcode files.

1. first.gcode, which is the normal gcode for the print, then modified to prevent the hotend from cooling down at the end.
2. repeat.gcode, which is the same as first.gcode, but also with the priming line removed, so that your printer doesn't try and print priming lines for every print.
3. eject.gcode, which is a gcode for your printer to push the part off the print bed.

When you run the python code, it will:

1. Tell octoprint to print first.gcode
2. Wait for the print to finish
3. Tell octoprint to print eject.gcode
4. Watch your camera for motion, if it doesn't see any motion within 30 seconds, exit.
5. Tell octoprint to print repeat.gcode
6. Go to 2.

Creating the GCode files:
---
Don't just use my gcode (also don't download and run gcode from other random people on the internet in general, you can damage your printer). My gcode is included as an example of how to edit your files, it is not meant for you to download run on your printer, especially if your printer is not the same as mine. Slice it as you would normally in your own slicer, then edit it using the instructions below.

I did this in Prusa Slicer, so this may differ to your slicer. After slicing the gcode open it in a text editor. Look for a line near the bottom of the file that looks like this:

```
M104 S0 ; turn off temperature
```

and remove it, this will prevent your hotend from cooling down. Save the file as first.gcode, but keep it open.

Next, head to the top of the file and look for two lines like this:

```
G1 X60.0 E9.0 F1000.0 ; intro line
G1 X100.0 E12.5 F1000.0 ; intro line
```

This is the part of the gcode responsible for the priming line. Change them to G0 and remove the E (for extrusion) and F (for feed rate).

```
G0 X60.0 ; intro line
G0 X100.0 ; intro line
```

They now simply become moves with no extrusion, which is handy for wiping any filament that leaks out of the hotend between prints onto the already existing priming line. Save the file as repeat.gcode

Next, we need to set up the ejection gcode. There's an example of my ejection gcode at example_gcode/eject.gcode. The technique I have used is to move the hotend to the back of the print, and slowly move it forwards and upwards to lift the print off the bed. This seems to work very well. You will need to adjust the values if you do not have a 25 x 21 x 21 cm bed.

Once you have your 3 gcode files, upload them all to octoprint.

Installation prep on a Raspberry PI (eg Octoprint):
---
I created a script to do most of the work for you on a Raspberry pi, just connect to your PI via SSH and run `curl https://gitlab.com/Azelphur/continuous-printing/-/raw/master/install-pi.sh | bash`

Setting up the code:
---
Copy config.yaml.example to config.yaml and edit the values as appropriate. Note that you should use the file name not the display name from octoprint. You can get the file name by pressing the download button in the file list in Octoprint.

Once you have done that, run `pip install -r requirements.txt`

Next, test that your camera / motion detection is working by running `python go.py --test-motion` (or `python go.py --test-motion-headless` if you are on a headless machine). Drop a shield in front of your camera to test it. If you get motion detected, it's working. If you don't get motion detected, try lowering motion_threshold. If you get motion detected when you shouldn't, check that nothing is moving in the cameras field of view, or increase motion_threshold.

Now you are ready to go. Run `python go.py` and it will begin printing. If you are already printing, feel free to run `python go.py --already-printing` to skip the first print, and wait for the current one to finish.
